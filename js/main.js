$(function () {
    $(window).scroll(function () {
        window.console.log($('.navbar').height());
        window.console.log($(window).scrollTop());
        var navbar = $('.navbar');
        if ($(window).scrollTop() >= navbar.height()){
            if (navbar.hasClass('scrolled')){
            } else {
                navbar.addClass('scrolled');
            }
        } else {
            navbar.removeClass('scrolled');
        }
    });
    $('.tab-switch li').click(function () {
        $(this).addClass('selected').siblings().removeClass('selected');
        $('.tabs-section .tabs-content > div').hide();
        // console.log($(this).data('class'));
        $('.' + $(this).data('class')).show();
    });
});